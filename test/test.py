import sys
from worker import task1, task2, task3
myFile = 'annual-rainfall-fortaleza-brazil.csv'

# Test Function 1
def task1_returns_list():
    assert type(task1(myFile)) == list
    
def task1_returns_list_of_dicts():
    for i in task1(myFile):
        assert type(i) == dict
        
def task1_dicts_three_keys():
    for i in task1(myFile):
        assert len(i.keys()) == 3

def task1_dicts_keys_types():
    for i in task1(myFile):
        for j in i.values():
            assert type(j) == int
            
def task1_dict_count():
    assert len(task1(myFile)) == 130
            
def task1_first_last():
    d = task1(myFile)
    assert d[0] == {'rainfall': 852, 'year': 1850, 'id': 0}
    assert d[-1] == {'id': 129, 'rainfall': 996, 'year': 1979}

task1_returns_list()
task1_returns_list_of_dicts()
task1_dicts_three_keys()
task1_dicts_keys_types()
task1_dict_count()
task1_first_last()

# Test Function 2
def task2_returns_list():
    assert type(task2()) == list
    
def task2_returns_list_of_dicts():
    for i in task2():
        assert type(i) == dict
        
def task2_dicts_three_keys():
    for i in task2():
        assert len(i.keys()) == 3

def task2_dicts_keys_types():
    for i in task2():
        for j in i.values():
            assert type(j) == int

def task2_no_inputs():
    assert task2() == task1(myFile)

def task2_left_input():
    assert task2(1893) == task1(myFile)[43:]

def task2_right_input():
    assert task2(end=1912) == task1(myFile)[:63]
    
def task2_left_right_input():
    assert task2(1893, 1912) == task1(myFile)[43:63]

def task2_left_gt_right_input():
    assert task2(1912, 1893) == []

def task2_left_eq_right_input():
    assert task2(1893, 1893) == task1(myFile)[43:44]

def task2_left_small_input():
    assert task2(-9999, 1893) == task1(myFile)[:44]

def task2_right_big_input():
    assert task2(1893, 9999) == task1(myFile)[43:]

task2_returns_list()
task2_returns_list_of_dicts()
task2_dicts_three_keys()
task2_dicts_keys_types()
task2_no_inputs()
task2_left_input()
task2_right_input()
task2_left_right_input()
task2_left_gt_right_input()
task2_left_eq_right_input()
task2_left_small_input()
task2_right_big_input()

# Test Function 3
def task3_returns_list():
    assert type(task3()) == list
    
def task3_returns_list_of_dicts():
    for i in task3():
        assert type(i) == dict
        
def task3_dicts_three_keys():
    for i in task3():
        assert len(i.keys()) == 3

def task3_dicts_keys_types():
    for i in task3():
        for j in i.values():
            assert type(j) == int

def task3_no_inputs():
    assert task3() == task1(myFile)

def task3_limit_input():
    assert task3(limit=15) == task1(myFile)[:15]
    
def task3_index_input():
    assert task3(index=15) == task1(myFile)[15:]
    
def task3_limit_index_input():
    assert task3(limit=15, index=20) == task1(myFile)[20:35]

def task3_limit_big_input():
    assert task3(limit=9999, index=3) == task3(index=3) 

def task3_limit_small_input():
    assert task3(limit=-1, index=15) == []

def task3_index_big_input():
    assert task3(index=9999) == []

def task3_index_small_input():
    assert task3(index=-100) == task1(myFile)[:-100]

task3_returns_list()
task3_returns_list_of_dicts()
task3_dicts_three_keys()
task3_dicts_keys_types()
task3_no_inputs()
task3_limit_input()
task3_index_input()
task3_limit_index_input()
task3_limit_big_input()
task3_limit_small_input()
task3_index_big_input()
task3_index_small_input()


