myFile = 'annual-rainfall-fortaleza-brazil.csv'

def task1(filename):
    f = open(filename)
    f.readline()
    counter = 0
    data = []
    for row in f:
        row = row.strip()
        myDict = {'id': counter, 'year': int(row[1:5]), 'rainfall': int(row[7:])}
        counter += 1
        data.append(myDict)
    return data

def task2(start = 1850, end = 1979):
    newData = []
    for i in task1(myFile):
        if start <= i['year'] <= end:
            newData.append(i)
    return newData

def task3(limit = len(task1(myFile)), index = 0):
    newData = []
    for i in task1(myFile):
        if index <= i['id'] <= index + limit - 1:
            newData.append(i)
    return newData

def interactive():
    print("What kind of query would you like to make?")
    response = input("Type 'a' for the whole dataset, 'b' for search by start and end year, or 'c' for search by limit and index")
    while (response not in ['a','b','c']):
        response = input("Please enter a valid choice")
    
    if (response == 'a'):
        print(task1(myFile))
    elif (response == 'b'):
        s = input("Start year: ")
        e = input("End year: ")
        try:
            s = int(s)
            e = int(e)
            print(task2(s,e))
        except:
            print("Invalid inputs")
    elif (response == 'c'):
        l = input("Limit: ")
        o = input("index: ")
        try:
            l = int(l)
            o = int(o)
            print(task3(l,o))
        except:
            print("Invalid inputs")
if __name__ == "__main__":
    interactive()
