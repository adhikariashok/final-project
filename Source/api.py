import os
from flask import Flask, jsonify, request
import sys
import json
import datetime
from worker import task1, task2, task3
from job import job_object, commit_job, grab_a_job, grab_all_job
myFile = 'annual-rainfall-fortaleza-brazil.csv'


app = Flask(__name__)
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0, decode_responses=True)


@app.route('/rain')
def rain():
    if request.args.get("start"):
        if "limit" in request.args or "offset" in request.args:
            return jsonify("Parameters mixed. Please keep start/end and limit/offset separated."), 400
        try:
            int(request.args.get("start"))
        except:
            return jsonify('Start Parameter not an Integer'), 400
        if request.args.get("end"):
            try:
                int(request.args.get("end"))
            except:
                return jsonify('End Parameter not an Integer'), 400
            return jsonify(task2(start = int(request.args.get("start")), end = int(request.args.get("end"))))
        return jsonify(task2(start = int(request.args.get("start"))))
    if request.args.get("end"):
        try:
            int(request.args.get("end"))
        except:
            return jsonify('End Parameter not an Integer'), 400
        return jsonify(task2(end = int(request.args.get("end"))))

    if request.args.get("limit"):
        if "start" in request.args or "end" in request.args:
            return jsonify("Parameters mixed. Please keep start/end and limit/offset separated."), 400
        try:
            int(request.args.get("limit"))
        except:
            return jsonify('Limit Parameter not an Integer'), 400
        if request.args.get("offset"):
            try:
                int(request.args.get("offset"))
            except:
                return jsonify('Offset Parameter not an Integer'), 400
            return jsonify(task3(limit = int(request.args.get("limit")), index = int(request.args.get("offset"))))
        return jsonify(task3(limit = int(request.args.get("limit"))))
    if request.args.get("offset"):
        try:
            int(request.args.get("offset"))
        except:
            return jsonify('Offset Parameter not an Integer'), 400
        return jsonify(task3(index = int(request.args.get("offset"))))

    return jsonify(task1(myFile))

@app.route('/rain/<int:id>')
def idsearch(id):
    return jsonify([d for d in task1(myFile) if d['id']==id])

@app.route('/rain/year/<int:year>')
def yearsearch(year):
    return jsonify([d for d in task1(myFile) if d['year']==year])

@app.route("/jobs", methods=["GET", "POST"])
def jobs():
    if request.method == "POST":
        json_req = request.get_json(force=True)

        start = json_req.get("start")
        end = json_req.get("end")
        limit = json_req.get("limit")
        offset = json_req.get("offset")

        if start:
            try:
                start=int(start)
            except:
                return "Start date must be Int"
            
        if end:
            try:
                end=int(end)
            except:
                return "End date must be Int"
        
        if limit:
            try:
                limit=int(limit)
            except:
                return "Limit must be Int"

        if offset:
            try:
                offset=int(offset)
            except:
                return "Offset must be Int"

        if (start or end) and (limit or offset):
            return "Parameters mixed. Please keep start/end and limit/offset separated."

        if start:
            if end:
                job = job_object(start, end)
                save = commit_job(job)
                if save == True:
                    pass
                else:
                    return "Error Processing Job"
            else:
                job = job_object(start, 1979)
                save = commit_job(job)
                if save == True:
                    pass
                else:
                    return "Error Processing Job"
        elif end:
            job = job_object(1850, end)
            save = commit_job(job)
            if save == True:
                pass
            else:
                return "Error Processing Job"
        elif limit:
            if offset:
                job = job_object(1850 + offset, 1850 + offset + limit)
                save = commit_job(job)
                if save == True:
                    pass
                else:
                    return "Error Processing Job"
            else:
                job = job_object(1850, 1850 + limit)
                save = commit_job(job)
                if save == True:
                    pass
                else:
                    return "Error Processing Job"
        elif offset:
            job = job_object(1850 + offset, 1979)
            save = commit_job(job)
            if save == True:
                pass
            else:
                return "Error Processing Job"
        
        return "Completed!"

    elif request.method == "GET":
        modify = []
        all_jobs = grab_all_job()
        for i in range(len(all_jobs)):
            all_ = json.loads(all_jobs[i])
            all_["current_time"] = str(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S'))
            modify.append(all_)
        final = {
            "jobs" : modify
        }
        return jsonify(final)

@app.route("/jobid/<id>")
def jobid(id):
    single = grab_a_job(id)
    if single == None:
        return f"Job with id {id} does not exist"
    else:
        modify = json.loads(single)
        modify["current_time"] = str(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S'))
        return jsonify(modify)

app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))
