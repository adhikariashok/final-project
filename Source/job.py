import uuid
import datetime
import json

import redis

redis_host = "172.17.0.1"
redis_port = 6379
redis_password = ""

mem = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, db=0, charset="utf-8", decode_responses=True)

def job_object(start=0, end=0):
    start_time = start
    end_time = end
    status = "submitted"
    time = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    try:
        int(start_time)
    except:
        return {"status" : "Error", "message": "end parameter must be an integer."}
    try:
        int(end_time)
    except:
        return {"status" : "Error", "message": "end parameter must be an integer."}
    if not start_time or not end_time:
        return {"status" : "Error", "message": "end parameter must be an integer."}
    id = str(uuid.uuid4())
    return {"id": id, "status": status, "start": start_time, "end": end_time, "last_updated" : str(time)}

def commit_job(job):
    try:
        if job["status"] == "Error":
            return job
        else:
            try:
                id = str(job["id"])
                rval = json.dumps(job)
                mem.set(id, rval)
                return True
            except Exception as e:
                print(e)
                return False
    except:
        return False

def grab_a_job(id):
    return mem.get(str(id))

def grab_all_job():
    jobs = []
    for key in mem.keys():
        jobs.append(mem.get(key))
    return jobs

if __name__ == '__main__':
    """job = job_object(1850, 1970)
    save = commit_job(job)
    if save:
        print(grab_a_job(job["id"]))
    else:
        print("error")"""
    single = grab_a_job("d8e8ae57-fc53-43da-89b9-80d4a67d6503")
    if single == None:
        print("False")
    else:
        s = json.loads(single)
        s["save"] = "Hello"
        print(single)
        print(s)
    # print([job for job in grab_all_job()])