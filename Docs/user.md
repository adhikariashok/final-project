This API is cloud based which allows access to data on rainfall in brazil from 1849-1979

The sunspots API uses a number of open source projects to work properly:

* Redis - database creation tool
* Flask - web-framework development tool
* Docker - containerization software

### Services

Our API currently provides the following services.

| Service | Endpoint |
| ------ | ------ |
| complete data set | GET /rainfall |
| data from single year | GET /rainfall/year/<year> |
| data from year with correspodning id | GET /rainfall/id/<id> |
| data from range of years | GET /rainfall?start=<year>&end=<year> |
| add a data point | POST /rainfall |
| list all jobs in queue | GET /jobs |
| submit a job | POST /jobs |


### Installing the API

The sunspots API is very easy to install and deploy in a Docker container. You need to make sure you have [installed Docker] and [Docker Compose].

Clone the sunspots repository:
```
$ git clone https://adhikariashok@bitbucket.org/adhikariashok/final-project.git
```

Start up the service using the docker-compose file:
```
$ cd rainfall
$ docker-compose up -d
```

* These instruction deploy the service to one machine. To deploy as a distributed service, see [these instructions.](deployment.md)

### Usage

Open your favorite Terminal and run these commands.

To list data from a range of years:

```
curl localhost:5000/rainfall?start=1849\&end=1865
```  
