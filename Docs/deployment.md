# Deployment to Multiple Machines

Deploying the API to multiple machines if fairly easy using Docker's swarm capabilities. The docker-compose-distributed.yml file is used to deploy the Flask API to one node, and the Redis database and worker functions to a different node. Note that the API and Redis database will both run seperate nodes (machines) while the load of the worker service will be distributed across all the worker nodes in the swarm.


### Dependencies

To deploy to multiple machines make sure you have [installed Docker] and [Docker Compose].

### Setup

Begin by logging in to a machine. This is the machine that will manage the swarm and where the Flask API will run. Clone the Final project repository.

Navigate to the repo and initiate the Docker swarm.

```
$ cd Final/ Project/
$ docker swarm init
```

You can add other machines, called worker nodes, to the swarm by copying the command that appears and running it on the node machines. __Note that only the swarm manager (the machine where you initiated the swarm) can run Docker commands__.

To view the machines that are part of your swarm, run:

```
$ docker node ls
```

Start up the sunspots service:
```
$ docker stack deploy -c docker-compose-distributed.yml rainfall
```

You can view the status of the service by running:
```
$ docker service ps
```

### Teardown

When you have finished using the service you can remove the service by running:
```
docker service rm rainfall
```

Run ``` docker service inspect rainfall``` to verify that the service has been shutdown.

To remove a node from the swarm run ``` docker node ls ``` and identify the name of the node you'd like to remove. Then run the following on the managing machine:
```
$ docker node rm NODE_NAME
```

To completley shutdown the swarm, you must remove all the worker nodes and then force the removal of the managing node by running the following on the swarm manager:
```
$ docker node rm --force
```



