This project utilizes JSON and Flask to build a http interface to a dataset containing annual rainfall in fortzela, Brazil from 1849-1979.
Utilizing Flask and JSON the interface user can input various endpoints and would get a JSON responses for effective communication.

Built by Ashok Adhikari for COE332 final project.
